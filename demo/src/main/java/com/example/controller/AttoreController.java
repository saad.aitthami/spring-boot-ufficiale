package com.example.controller;

import com.example.component.MiaClasse;
import com.example.domain.Attore;
import com.example.service.AttoreService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

@RestController
@RequestMapping("/api/attori")
@CrossOrigin(origins = "*", maxAge = 3600 )
public class AttoreController {
    AttoreService attoreService;
    MiaClasse miaClasse;
    public AttoreController(AttoreService attoreService)
        {
            this.attoreService=attoreService;
            this.miaClasse= new MiaClasse(); 
        }
    
    @GetMapping("/component/c")
        public ResponseEntity<?> c()
        {
            return new ResponseEntity<String>(miaClasse.getTitolo(),HttpStatus.OK);
        }



    @GetMapping()
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<List<Attore>>(attoreService.FindAll(),HttpStatus.OK);
    }
    @GetMapping("/{codAttore}")
    public ResponseEntity<?> findById(@PathVariable("codAttore") Long codAttore)
        {
            Optional<Attore> optAttore= attoreService.findById(codAttore);
            if(optAttore.isPresent())
                {
                    Attore attore = optAttore.get();
                    return new ResponseEntity<Attore>(attore,HttpStatus.OK);
                }
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
    @PostMapping()
    public ResponseEntity<?> save(@RequestBody Attore attore)
        {
            attoreService.save(attore);
            return new ResponseEntity<Void>(HttpStatus.OK);
        }

}


